﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_Lab_1
{
    internal class Program5
    {
        static void Main(string[] args)
        {
            Days day1 = Days.Monday;
            Days day2 = Days.Tuesday;

            Console.WriteLine("day1 == day2: " + (day1 == day2)); // false

            // Порівняння з однією з констант перелічуваного типу
            Console.WriteLine("day1 == Days.Monday: " + (day1 == Days.Monday)); // true

            // Порівняння зі значенням, яке не є частиною перелічуваного типу
            Console.WriteLine("day1 == (Days)2: " + (day1 == (Days)2)); // false

            // Використання булівських операцій
            // Логічне І
            Console.WriteLine("day1 == Days.Monday && day2 == Days.Tuesday: " + (day1 == Days.Monday && day2 == Days.Tuesday)); // true

            // Логічне АБО
            Console.WriteLine("day1 == Days.Monday || day2 == Days.Tuesday: " + (day1 == Days.Monday || day2 == Days.Tuesday)); // true

            // Побітове І
            Console.WriteLine("day1 & Days.Tuesday: " + (day1 & Days.Tuesday)); // 0 (не true, не false)

            // Побітове АБО
            Console.WriteLine("day1 | Days.Monday: " + (day1 | Days.Wednesday)); // 2

            // Побітове виключне АБО
            Console.WriteLine("day1 ^ Days.Tuesday: " + (day1 ^ Days.Tuesday)); // 1
        }

    }
    public enum Days
    {
        Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday
    }

}
