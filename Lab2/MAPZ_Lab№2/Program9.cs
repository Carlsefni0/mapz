﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_Lab_1
{
    class Program9
    {
        static void FunctionWithOut(out int result)
        {
            result = 10;
        }

        static void FunctionWithRef(ref int value)
        {
            value *= 2;
        }



        static void Main(string[] args)
        {
            int outResult;
            FunctionWithOut(out outResult);
            Console.WriteLine("Result from FunctionWithOut: " + outResult);

            int refValue = 5;
            Console.WriteLine("Value before FunctionWithRef: " + refValue);
            FunctionWithRef(ref refValue);
            Console.WriteLine("Value after FunctionWithRef: " + refValue);

            //У випадку коли необхідно повернути кілька значень з функції, параметри ref/out можуть бути взаємозаміннми


        }
    }
}
