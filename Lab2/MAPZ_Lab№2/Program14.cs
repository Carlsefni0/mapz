﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_Lab_1
{
    internal class Program14
    {
        static void Main(string[] args)
        {
            Animal animal1 = new Animal("Cat", 3);
            Animal animal2 = new Animal("Dog", 5);
            Animal animal3 = new Animal("Cat", 3);

            // Перевірка на рівність об'єктів за допомогою метода Equals
            Console.WriteLine("animal1 equals animal2: " + animal1.Equals(animal2)); // Виведе False
            Console.WriteLine("animal1 equals animal3: " + animal1.Equals(animal3)); // Виведе True

            // Виведення хеш-кодів об'єктів за допомогою метода GetHashCode
            Console.WriteLine("animal1 hash code: " + animal1.GetHashCode());
            Console.WriteLine("animal2 hash code: " + animal2.GetHashCode());
            Console.WriteLine("animal3 hash code: " + animal3.GetHashCode());

            // Виведення рядкового представлення об'єктів за допомогою метода ToString
            Console.WriteLine("animal1: " + animal1);
            Console.WriteLine("animal2: " + animal2);
            Console.WriteLine("animal3: " + animal3);

            Console.WriteLine("Type of animal1: " + animal1.GetType());
        }
    }
    class Animal
    {
        public string Name { get; set; }
        public int Age { get; set; }

        public Animal(string name, int age)
        {
            Name = name;
            Age = age;
        }

        public override string ToString()
        {
            return $"Animal: Name={Name}, Age={Age}";
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode() ^ Age.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is Animal))
            {
                return false;
            }
            var otherAnimal = (Animal)obj;
            return Name == otherAnimal.Name && Age == otherAnimal.Age;
        }




    }
}
