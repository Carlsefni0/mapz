﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_Lab_1
{
    Заміна class на struct спричинить масу помилок, оскільки структури не можуть успадковувати абстрактні класи
    та protected атрибути
    internal class Program12
    {
        static void Main(string[] args)
        {
            FullStack John = new FullStack();

            John.beBackendDev();
            John.beFrontendDev();

        }


    }
    public interface IFrontendDev
    {
        void beFrontendDev();

    }
    public abstract struct BackendDev
    {
        protected string name;
        protected int age;

        public abstract void beBackendDev() { }
    }

    public struct FullStack : BackendDev, IFrontendDev
    {
        public FullStack(string name, int age) { Console.WriteLine("Hi! My name is " + this.name); }

        public void beFrontendDev() { Console.WriteLine("I know React"); }
        public void beBackendDev() { Console.WriteLine("I know Java"); }
    }

}

