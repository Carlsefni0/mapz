﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_Lab_1
{
    internal class Program6
    {
        static void Main(string[] args)
        {
            FullStack John = new();

            John.beBackendDev();
            John.beFrontendDev();

        }

        public interface IFrontendDev
        {
            void beFrontendDev();

        }
        public interface IBackendDev
        {
            void beBackendDev();
        }

        public class FullStack : IFrontendDev, IBackendDev
        {
            public FullStack() { Console.WriteLine("Hi! I'm looking for a job"); }

            public void beFrontendDev() { Console.WriteLine("I know React"); }
            public void beBackendDev() { Console.WriteLine("I know Java"); }

        }

    }


}
