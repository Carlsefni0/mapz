﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Running;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAZP_Lab_1
{
    [MemoryDiagnoser]
    public class BenchMark
    {

        private int N = 1000000;

        private int intValue = 10;
        private object intBox = 10;

        [Benchmark]
        public void BoxingInt()
        {
            for (int i = 0; i < N; i++)
            {
                intBox = intValue;
            }
        }

        [Benchmark]
        public void UnboxingInt()
        {
            for (int i = 0; i < N; i++)
            {
                intValue = (int)intBox;
            }
        }

        int assignedInt = 0;

        [Benchmark]
        public void AssignInt()
        {
            for (int i = 0; i < N; i++)
            {
                assignedInt = intValue;
            }
        }


        private double doubleValue = 10.5;
        private object doubleBox = 10.4;

        [Benchmark]
        public void BoxingDouble()
        {
            for (int i = 0; i < N; i++)
            {
                doubleBox = doubleValue;
                doubleValue = (double)doubleBox;
            }
        }

        [Benchmark]
        public void UnboxingDouble()
        {
            for (int i = 0; i < N; i++)
            {
                doubleValue = (double)doubleBox;
            }
        }

        double assignedDouble = 0;

        [Benchmark]
        public void AssignDouble()
        {
            for (int i = 0; i < N; i++)
            {
                assignedDouble = doubleValue;
            }
        }


        struct SmallStruct
        {
            public double Value1;
        }

        struct MediumStruct
        {
            public double Value1;
            public double Value2;
            public double Value3;
        }

        struct LargeStruct
        {
            public double Value1;
            public double Value2;
            public double Value3;
            public double Value4;
            public double Value5;
            public double Value6;
        }


        SmallStruct small = new SmallStruct();
        object? boxStructure1;

        MediumStruct medium = new MediumStruct();
        object? boxStructure2;

        LargeStruct large = new LargeStruct();
        object? boxStructure3;

        [Benchmark]
        public void SmallStructBox()
        {
            small.Value1 = 10.2;
            for (int i = 0; i < N; i++)
            {
                boxStructure1 = small;
            }
        }

        [Benchmark]
        public void SmallStructUnBox()
        {
            small.Value1 = 10.2;
            boxStructure1 = small;
            for (int i = 0; i < N; i++)
            {
                small = (SmallStruct)boxStructure1;
            }
        }


        [Benchmark]
        public void MediumStructBox()
        {
            medium.Value1 = 10.3;
            medium.Value2 = 20.3;
            medium.Value3 = 30.3;
            for (int i = 0; i < N; i++)
            {
                boxStructure2 = medium;
            }
        }
        [Benchmark]
        public void MediumStructUnBox()
        {
            medium.Value1 = 10.3;
            medium.Value2 = 20.3;
            medium.Value3 = 30.3;
            boxStructure2 = medium;
            for (int i = 0; i < N; i++)
            {
                medium = (MediumStruct)boxStructure2;
            }
        }

        [Benchmark]
        public void LargeStructBox()
        {
            large.Value1 = 10.3;
            large.Value2 = 20.3;
            large.Value3 = 30.3;
            large.Value4 = 40.3;
            large.Value5 = 50.3;
            large.Value6 = 60.3;
            for (int i = 0; i < N; i++)
            {
                boxStructure3 = large;
            }
        }

        [Benchmark]
        public void LargeStructUnBox()
        {
            large.Value1 = 10.3;
            large.Value2 = 20.3;
            large.Value3 = 30.3;
            large.Value4 = 40.3;
            large.Value5 = 50.3;
            large.Value6 = 60.3;
            boxStructure3 = large;
            for (int i = 0; i < N; i++)
            {
                large = (LargeStruct)boxStructure3;
            }
        }
    }
    public class Program
    {
        static void Main(string[] args)
        {
            BenchmarkRunner.Run<BenchMark>();
        }
    }
}
