﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_Lab_1
{
    internal class Program10
    {
        static void Main(string[] args)
        {
            int number = 42;
            object boxedNumber = number;

            Console.WriteLine("Boxed number: " + boxedNumber);

            int unboxedNumber = (int)boxedNumber;

            Console.WriteLine("Unboxed number: " + unboxedNumber);
        }
    }
}
