﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_Lab_1
{
    internal class Program11
    {
        static void Main(string[] args)
        {
            Demo Demo1 = new Demo(1000);
            double meters = (double)Demo1;
            Console.WriteLine("Demo in meters: " + meters);

            double DemoValue = 2000;
            Demo Demo2 = DemoValue;
            Console.WriteLine("Demo in meters: " + (double)Demo2);
        }
    }
    class Demo
    {
        private double meters;

        public Demo(double meters)
        {
            this.meters = meters;
        }

        public static explicit operator double(Demo d)
        {
            return d.meters / 1000;
        }

        public static implicit operator Demo(double km)
        {
            return new Demo(km * 1000);
        }
    }
}
