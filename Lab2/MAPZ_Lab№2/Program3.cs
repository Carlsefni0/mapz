﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_Lab_1
{


    class Program3
    {
        static void Main()
        {
            Demo obj = new Demo(20);
            Demo.NestedClass = new Demo.NestedDemo();
            Demo.value
        }

    }

    //public
    //будь-який клас може реалізовувати інтерфейс
    interface IDemo
    {
        void IDemoMethod();
    }
    //internal
    class Demo
    {
        //private
        int value;

        public Demo(int value) { Console.WriteLine("Demo object was created with value: " + value); }

        //private
        void DemoMethod() { Console.WriteLine("Demo object value is " + this.value); }



        //private
        class NestedDemo
        {
            NestedDemo() { Console.WriteLine("Nested object was created"); }
        }
    }

}
