﻿namespace MAPZ_Lab_1
{
    internal class Program2
    {
        static void Main(string[] args)
        {
            Demo obj = new Demo();
            obj.PublicMethod();
            obj.ProtectedInternalMethod();
            obj.InternalMethod();

            Console.WriteLine("The next object is inherited from Demo");
            InheritedDemo obj2 = new InheritedDemo();
            obj2.ProtectedInternalMethod();
            obj2.InternalMethod();
            obj2.InheritedProtectedMethod();


        }
    }
    public class Demo
    {
        private string privateString = "I'm private";
        public string publicString = "I'm public";
        protected string protectedString = "I'm protected";
        internal string internalString = "I'm internal";
        protected internal string protectedInternalString = "I'm protected internal";

        public Demo()
        {
            Console.WriteLine("Constructor: " + privateString);
        }
        public void PublicMethod()
        {
            Console.WriteLine("Public method: " + privateString);
        }

        private void PrivateMethod()
        {
            Console.WriteLine("Private method: " + privateString);
        }

        protected void ProtectedMethod()
        {
            Console.WriteLine("Protected method: " + privateString);
        }

        internal void InternalMethod()
        {
            Console.WriteLine("Internal method: " + privateString);
        }

        protected internal void ProtectedInternalMethod()
        {
            Console.WriteLine("Protected internal method: " + privateString);
        }
    }
    public class InheritedDemo : Demo
    {
        public InheritedDemo()
        {
            Console.WriteLine("I'm inherited class with public constructor: " + this.protectedString);
        }
        public void InheritedProtectedMethod()
        {
            this.ProtectedMethod();
        }

    }
}
