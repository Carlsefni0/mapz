﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_Lab_1
{
    internal class Program4
    {
        static void Main()
        {
            OuterClass outerInstance = new OuterClass();

            // Немає доступу до внутрішнього класу ззовні
            // OuterClass.InnerClass innerInstance = new OuterClass.InnerClass(); 

            // Немає доступу до поля внутрішнього класу ззовні
            outerInstance.innerInstance.InnerMethod(); // Помилка компіляції

            // Але ми можемо викликати метод, який має доступ до внутрішнього класу
            outerInstance.AccessInnerClass();
        }
        public class OuterClass
        {
            private class InnerClass
            {
                public void InnerMethod()
                {
                    Console.WriteLine("Inner method called.");
                }
            }

            // Поле внутрішнього класу
            private InnerClass innerInstance = new InnerClass();

            // Метод для демонстрації доступу до внутрішнього класу та його методу
            public void AccessInnerClass()
            {
                // Доступ до методу внутрішнього класу
                innerInstance.InnerMethod();
            }
        }
    }
}
