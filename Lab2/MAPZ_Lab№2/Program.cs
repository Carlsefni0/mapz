﻿namespace MAPZ_Lab_1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Developer John = new Developer("John");
            John.myJob();
            John.myTask();

        }
    }
    public interface ITask
    {
        void myTask();
    }
    public abstract class Worker
    {
        void myJob() { }
    }
    public class Developer : Worker, ITask
    {
        string name;

        public Developer(string name)
        {
            this.name = name;
        }
        public void myTask()
        {
            Console.WriteLine("My task is to create design");
        }
        public void myJob()
        {
            Console.WriteLine("I'm a frontend developer");
        }
    }

}
