﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_Lab_1
{
    internal class Program8
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Creating instance 1:");
            InitializationOrder instance1 = new InitializationOrder(1);
            instance1.PrintFields();
            Console.WriteLine();

            Console.WriteLine("Creating instance 2:");
            InitializationOrder instance2 = new InitializationOrder(2);
            instance2.SetDynamicFieldValue("New dynamic value");
            instance2.PrintFields();
        }
    }

    class InitializationOrder
    {
        static string staticField = "Static field";
        string dynamicField;
        int instanceNumber;

        static InitializationOrder()
        {
            Console.WriteLine("Static constructor called.");
        }

        public InitializationOrder(int instanceNumber)
        {
            this.instanceNumber = instanceNumber;
            dynamicField = "Dynamic field";
            Console.WriteLine($"Dynamic constructor called for instance {instanceNumber}.");
        }

        public void SetDynamicFieldValue(string value)
        {
            dynamicField = value;
        }

        public void PrintFields()
        {
            Console.WriteLine($"Instance {instanceNumber}:");
            Console.WriteLine("Static field value: " + staticField);
            Console.WriteLine("Dynamic field value: " + dynamicField);
        }
    }
}
