﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_Lab_1
{
    internal class Program7
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Creating BaseClass object:");
            BaseClass baseObj1 = new BaseClass();
            Console.WriteLine();

            Console.WriteLine("Creating BaseClass object with parameter:");
            BaseClass baseObj2 = new BaseClass(10);
            Console.WriteLine();

            Console.WriteLine("Creating DerivedClass object:");
            DerivedClass derivedObj1 = new DerivedClass();
            Console.WriteLine();

            Console.WriteLine("Creating DerivedClass object with parameters:");
            DerivedClass derivedObj2 = new DerivedClass(20, 30);
            Console.WriteLine();

            Console.WriteLine("Calling Method on BaseClass object:");
            baseObj1.Method();
            Console.WriteLine();

            Console.WriteLine("Calling Method on DerivedClass object:");
            derivedObj1.Method();

        }
    }
    public class BaseClass
    {
        protected int baseValue;
        public BaseClass()
        {
            this.baseValue = 0;
            Console.WriteLine("Default base value: " + this.baseValue);
        }
        public BaseClass(int baseValue)
        {
            this.baseValue = baseValue;
            Console.WriteLine("Custom base value: " + this.baseValue);
        }
        public virtual void Method()
        {
            Console.WriteLine("BaseClass: Method()");
        }
    }
    public class DerivedClass : BaseClass
    {
        private int derivedValue;

        public DerivedClass() : base()
        {
            Console.WriteLine("Default derived value: " + base.baseValue);
        }

        public DerivedClass(int baseValue, int derivedValue) : base(baseValue)
        {
            this.derivedValue = derivedValue;
            Console.WriteLine("Custom derived value: " + baseValue + ", " + derivedValue);
        }
        public override void Method()
        {
            Console.WriteLine("DerivedClass: Method()");
            base.Method();
        }
    }
}
