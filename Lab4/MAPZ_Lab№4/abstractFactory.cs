﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace MAPZ_Lab_4
{
    
    public static class MutantNameGenerator
    {
        private static readonly List<string> Names = new List<string>
        {
            "Xavier", "Magneto", "Storm", "Wolverine", "Jean Grey", "Cyclops", "Rogue", "Beast", "Gambit"
        };

        public static string GenerateRandomName()
        {
            Random random = new Random();
            int index = random.Next(Names.Count);
            return Names[index];
        }
    }

   
    public interface IMutantSkill
    {
        string UseSkill();
    }
  
  

    public abstract class Mutant: IMutantSkill, ICloneable
    {
        protected string name { get; set; }
        protected int damage { get; set; }
        protected int hp { get; set; }
        protected int intelligence { get; set; }

        

        public Mutant(string name, int damage, int hp, int intelligence)
        {
            this.name = name;
            this.damage = damage;   
            this.hp = hp;
            this.intelligence = intelligence;
            
        }

       

        public virtual string ShowInfo()
        {
            return $"Ви створили мутанта з такими характеристиками:\nІм'я: {name} \nУрон: {damage}\nHP: {hp}\nІнтелект: {intelligence}";
        }
        public virtual string UseSkill()
        {
            return "Skill";
        }
        public abstract object Clone();
       
    }
    //Абстрактна фабрика яка генерує 3 типи мутантів
    public interface IAbstractMutantFactory
    {
        Mutant CreateTankMutant();
        Mutant CreateDamagerMutant();
        Mutant CreateSupportMutant();
    }

    //Фабрика яка генерує мутантів з ближнім типом атак
    public class MeleeMutantFactory :  IAbstractMutantFactory
    {
        public Mutant CreateTankMutant()
        {
            Random random = new Random();

            return new MeleeTankMutant(name: MutantNameGenerator.GenerateRandomName(), damage: random.Next(50, 101), hp: random.Next(500, 1001), intelligence: random.Next(1, 11));
        }

        public Mutant CreateDamagerMutant()
        {
            Random random = new Random();

            return new MeleeTankMutant(name: MutantNameGenerator.GenerateRandomName(), damage: random.Next(50, 101), hp: random.Next(500, 1001), intelligence: random.Next(1, 11));
        }

        public Mutant CreateSupportMutant()
        {
            Random random = new Random();

            return new MeleeTankMutant(name: MutantNameGenerator.GenerateRandomName(), damage: random.Next(50, 101), hp: random.Next(500, 1001), intelligence: random.Next(1, 11));
        }
       




    }
    //Фабрика яка генерує мутантів з дальнім типом атак
    public class RangeMutantFactory : IAbstractMutantFactory
    {


        public Mutant CreateTankMutant()
        {
            Random random = new Random();

            return new RangeTankMutant(name: MutantNameGenerator.GenerateRandomName(), damage: random.Next(50, 101), hp: random.Next(500, 1001), intelligence: random.Next(1, 11));
        }

        public Mutant CreateDamagerMutant()
        {
            Random random = new Random();

            return new RangeDamagerMutant(name: MutantNameGenerator.GenerateRandomName(), damage: random.Next(50, 101), hp: random.Next(500, 1001), intelligence: random.Next(1, 11));
        }

        public Mutant CreateSupportMutant()
        {
            Random random = new Random();

            return new RangeDamagerMutant(name: MutantNameGenerator.GenerateRandomName(), damage: random.Next(50, 101), hp: random.Next(500, 1001), intelligence: random.Next(1, 11));
        }

        
    }

    //Інтерфейси що визначають здібності мутантів
    //public interface IAbstractTankMutant: IMutantSkill
    //{
    //    string ShowInfo();

    //}

    //public interface IAbstractDamagerMutant: IMutantSkill
    //{
    //    string ShowInfo();


    //}

    //public interface IAbstractSupportMutant: IMutantSkill
    //{
    //    string ShowInfo();


   // }


    //Реалізація здібностей для різних типів мутанта танка
    public class MeleeTankMutant : Mutant { 

        public MeleeTankMutant(string name, int damage, int hp, int intelligence) : base(name,damage, hp, intelligence) { }
      
        public override object Clone()
        {
            return new MeleeTankMutant(this.name, this.damage, this.hp, this.intelligence);
        }

        public override string UseSkill() {
            return "Мутант таранить ворогів ";
        }

    }

    public class RangeTankMutant : Mutant
    {
        public  RangeTankMutant(string name, int damage, int hp, int intelligence) : base(name, damage, hp, intelligence) { }
        public override object Clone()
        {
            return new RangeTankMutant(this.name, this.damage, this.hp, this.intelligence);
        }
        public override string UseSkill()
        {
            return "Мутант вдаряє по землі та наносить урон ударною хвилею";
        }

      
    }
    //Реалізація здібностей для різних типів мутанта ініціатора
    public class MeleeDamagerMutant : Mutant 
    {
        public  MeleeDamagerMutant(string name, int damage, int hp, int intelligence) : base(name, damage, hp, intelligence) { }
        public override object Clone()
        {
            return new MeleeDamagerMutant(this.name, this.damage, this.hp, this.intelligence);
        }
        public override string UseSkill()
        {
            return "Мутант вривається в натовп ворогів та завдає шкоди кожному противнику";
        }

    }

    public class RangeDamagerMutant : Mutant
    {
        public RangeDamagerMutant(string name, int damage, int hp, int intelligence) : base(name, damage, hp, intelligence) { }
        public override object Clone()
        {
            return new RangeDamagerMutant(this.name, this.damage, this.hp, this.intelligence);
        }
        public override string UseSkill()
        {
            return "Мутант випускає потік розпеченої кислоти у всіх противників";
        }

    }
    //Реалізація здібностей для різних типів мутанта підтримки

    public class MeleeSupportMutant : Mutant
    {
        public  MeleeSupportMutant(string name, int damage, int hp, int intelligence) : base(name, damage, hp, intelligence) { }
        public override object Clone()
        {
            return new MeleeSupportMutant(this.name, this.damage, this.hp, this.intelligence);
        }
        public override string UseSkill()
        {
            return "Мутант через щупальця встановлює контакт із союзниками та збільшує їх регенерацію";

        }

    }

    public class RangeSupportMutant : Mutant
    {

        public RangeSupportMutant(string name, int damage, int hp, int intelligence) : base(name, damage, hp, intelligence) { }
        public override object Clone()
        {
            return new RangeSupportMutant(this.name, this.damage, this.hp, this.intelligence);
        }
        public override string UseSkill()
        {
            return "Мутант генерує негативну ауру яка зменшую спротив ворогів до негативних ефектів";

        }

    }
}