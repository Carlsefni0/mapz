﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_Lab_4
{
    public class ResourceManager
    {
        private static ResourceManager _instance;
        private int _money;
        private int _dna;
        private int _essence;

        private ResourceManager()
        {
            _money = 50000;
            _dna = 1000;
            _essence = 5;
        }

        // Глобальна точка доступу до єдиного екземпляра класу ResourceManager
        public static ResourceManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ResourceManager();
                }
                return _instance;
            }
        }

        public void AddMoney(int amount)
        {
            _money += amount;
        }

        public bool SpendMoney(int amount)
        {
            if (_money >= amount)
            {
                _money -= amount;
                return true;
            }
            return false;
        }

        public void AddDNA(int amount)
        {
            _dna += amount;
        }

        public bool SpendDNA(int amount)
        {
            if (_dna >= amount)
            {
                _dna -= amount;
                return true;
            }
            return false;
        }

        public void AddEssence(int amount)
        {
            _essence += amount;
        }

        public bool SpendEssence(int amount)
        {
            if (_essence >= amount)
            {
                _essence -= amount;
                return true;
            }
            return false;
        }

        public int GetMoney()
        {
            return _money;
        }

        public int GetDNA()
        {
            return _dna;
        }

        public int GetEssence()
        {
            return _essence;
        }
    }
}
