﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MAPZ_Lab_4
{
    /// <summary>
    /// Interaction logic for Laboratory.xaml
    /// </summary>
    public partial class Laboratory : Window
    {
        //private object mutant;
        private List<Mutant> mutants = new List<Mutant>();
        private IAbstractMutantFactory mutantFactory;
        private Mutant mutant;
     

        public Laboratory(IAbstractMutantFactory factory)
        {
            InitializeComponent();
            this.mutantFactory = factory;

            var resourceManager = ResourceManager.Instance;

            MoneyAmount.Text = resourceManager.GetMoney().ToString();
            DNAAmount.Text = resourceManager.GetDNA().ToString();
            EssenceAmount.Text = resourceManager.GetEssence().ToString();



        }
        public  bool SpendResources(int moneyAmount, int dnaAmount, int essenceAmount  )
        {
            var resourceManager = ResourceManager.Instance;


            if (resourceManager.GetDNA() <= 0 || resourceManager.GetEssence() <= 0 || resourceManager.GetMoney() <= 10)
            {
                MessageBox.Show("Недостатньо ресурсів для виконання клонування");
                return false;
            }

            resourceManager.SpendMoney(moneyAmount);
            resourceManager.SpendEssence(essenceAmount);
            resourceManager.SpendDNA(dnaAmount);

            MoneyAmount.Text = resourceManager.GetMoney().ToString();
            DNAAmount.Text = resourceManager.GetDNA().ToString();
            EssenceAmount.Text = resourceManager.GetEssence().ToString();
            return true;
        }

      

        private void Button_Click(object sender, RoutedEventArgs e)
        {


            if(this.mutant == null) {
                MessageBox.Show("Немає мутантів доступних для клонування. Створіть хоча б одного", "Помилка");
                return;
            }
            SpendResources(10000,100,1);
            Mutant clonedMutant = (Mutant)(this.mutant).Clone();
            MessageBox.Show(clonedMutant.ShowInfo() + "\nОсоблива здібність: " + clonedMutant.UseSkill(), "Клонування успішне");

            this.mutants.Add(clonedMutant);

        }

       

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if(SpendResources(10000, 75, 0))
            {
                var mutant = this.mutantFactory.CreateTankMutant();
                MutantInfo.Text = mutant.ShowInfo() + "\nОсоблива здібність: " + mutant.UseSkill();

                this.mutant = mutant;
            }
           
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            if(SpendResources(10000, 75, 0))
            {
                var mutant = this.mutantFactory.CreateDamagerMutant();
                MutantInfo.Text = mutant.ShowInfo() + "\nОсоблива здібність: " + mutant.UseSkill();

                this.mutant = mutant;
            }



        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            if(SpendResources(10000, 75, 0))
            {

                var mutant = this.mutantFactory.CreateSupportMutant();
                MutantInfo.Text = mutant.ShowInfo() + "\nОсоблива здібність: " + mutant.UseSkill();

                this.mutant = mutant;
            }

        }

    }
}
