﻿using System.Windows;
using System.Windows.Navigation;


//рандомізація на вихід + 
//Прототип винести ICloneAble, Глибоке копіювання +
//Винести клас (танк сапорт дамагер) мутанта у лабораторію (лабораторія приймає фабрику в конструктор і вже створює мутанта) +
//Уніфікувати методи скілів +


namespace MAPZ_Lab_4
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (UserNameTextBox.Text == "" || UserNameTextBox.Text == "User Name")
            {
                MessageBox.Show("Введіть ім'я користувача");
                return;
            }
            string userName = UserNameTextBox.Text;

            CreatingMutant creatingMutant = new CreatingMutant(userName);
            creatingMutant.Show();
            this.Close();
        }
    }
}