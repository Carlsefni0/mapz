﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MAPZ_Lab_4
{
    /// <summary>
    /// Interaction logic for CreatingMutant.xaml
    /// </summary>
    public partial class CreatingMutant : Window
    {
        string userName = string.Empty;
        public CreatingMutant(string userName)
        {
            InitializeComponent();

            GreetingTextBlock.Text = "Прокинувся, " + userName + "? Давай швидко розбивай капсулу з тою потворою! На нас ворожі мутанти напали";
        }



        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if ( AttackTypeListBox.SelectedItem == null)
            {
                MessageBox.Show("Оберіть тип мутанта та тип атаки.");
                return;
            }


            string attackType = ((ListBoxItem)AttackTypeListBox.SelectedItem).Content.ToString();




            IAbstractMutantFactory factory;
            if (attackType == "Ближній тип мутанта")
            {
                factory = new MeleeMutantFactory();
            }
            else
            {
                factory = new RangeMutantFactory();
            }
            Laboratory lab = new Laboratory(factory);
            lab.Show();

           
            this.Close();

        }
    }
}
