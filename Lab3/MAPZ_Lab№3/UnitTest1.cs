using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using Xunit;
using static MAPZ_Lab3.UnitTest1;

namespace MAPZ_Lab3
{
    public static class MyLinqExtensions
    {
        public static IEnumerable<Mutant> SortByLevel(this IEnumerable<Mutant> mutants)
        {
            return mutants.OrderBy(m => m.level);
        }
    }
    //sort -> orderby
    //comprar ��� �������� null

    public class UnitTest1
    {


        public class Mutant
        {
            public double damage { get; set; }
            public string name { get; set; }
            public bool ableToFight { get; set; }
            public int level { get; set; }
            public int mutantClassID { get; set; }

            public Mutant(double damage, string name, bool ableToFight, int level, int mutantClassID)
            {
                this.damage = damage;
                this.name = name;
                this.ableToFight = ableToFight;
                this.level = level;
                this.mutantClassID = mutantClassID;
            }
            public Mutant()
            {
            }
        }
        public class MutantClass
        {
            public string name { get; set; }
            public int id { get; set; }


        }

        class MutantDamageComparer : IComparer<Mutant>
        {
            public int Compare(Mutant x, Mutant y)
            {
                if (x == null && y == null)
                    return 0;
                if (x == null)
                    return -1;
                if (y == null)
                    return 1;


                return x.damage.CompareTo(y.damage);
            }
        }
       

        public List<MutantClass> mutantClasses
        {
            get
            {
                return new List<MutantClass>()
                {
                    new MutantClass() { name = "Tank", id = 1 },
                    new MutantClass() { name = "Damager", id = 2 },
                    new MutantClass() { name = "Support", id = 3 },
                };

            }

        }
        public List<Mutant> mutants
        {
            get
            {
                return new List<Mutant>()
                {
                    new Mutant(39.47, "Cyclop", false, 13,1),
                    new Mutant(129.99,"Ninja Turtle", true, 23,2),
                    new Mutant(50.29, "Mindmeld", true, 17,3),
                    new Mutant(79.80, "Bio-morph", false, 20,2),
                    new Mutant(101.2, "Frostbite", true, 21,2)
                };
            }
        }
        [Fact]
        public void Test1()
        {
            

            PerformSelect(mutants);

        }
        [Fact]
        public void Test2()
        {
           


            PerformWhere(mutants);
        }


        [Fact]
        public void Test3()
        {
            Dictionary<int, Mutant> mutantTeam = new Dictionary<int, Mutant>();

            mutantTeam.Add(1, new Mutant(39.47, "Cyclop", false, 13, 1));
            mutantTeam.Add(2, new Mutant(129.99, "Ninja Turtle", true, 23, 2));
            mutantTeam.Add(3, new Mutant(50.29, "Mindmeld", true, 17, 3));
            mutantTeam.Add(4, new Mutant(79.80, "Bio-morph", false, 20, 2));
            mutantTeam.Add(5, new Mutant(101.2, "Frostbite", true, 21, 2));

            PerformSelect(mutants);
            PerformWhere(mutants);
        }
        [Fact]
        public void Test4()
        {
            //List<Mutant> mutantTeam = new List<Mutant>();

            //mutantTeam.Add(new Mutant(39.47, "Cyclop", false, 13, 1));
            //mutantTeam.Add(new Mutant(129.99, "Ninja Turtle", true, 23, 2));
            //mutantTeam.Add(new Mutant(50.29, "Mindmeld", true, 17, 3));
            //mutantTeam.Add(new Mutant(79.80, "Bio-morph", false, 20, 2));
            //mutantTeam.Add(new Mutant(101.2, "Frostbite", true, 21, 2));

            var sortedMutants = mutants.SortByLevel().ToList();


            Assert.Equal("Cyclop", sortedMutants.First().name);

            Assert.Equal("Ninja Turtle", sortedMutants.Last().name);


        }
        [Fact]
        public void Test5()
        {
            List<Mutant> library = new List<Mutant>
                {
                    new Mutant{ damage = 39.47, name = "Cyclop", ableToFight = false, level = 13, mutantClassID = 1 },
                    new Mutant{ damage = 129.99, name = "Ninja Turtle", ableToFight = true, level = 23, mutantClassID = 2 },
                    new Mutant{ damage = 50.29, name = "Mindmeld", ableToFight = true, level = 17, mutantClassID = 3 },
                    new Mutant{ damage = 79.80, name = "Bio-morph", ableToFight = false, level = 20, mutantClassID = 2 },
                    new Mutant{ damage = 101.2, name = "Frostbite", ableToFight = true, level = 21, mutantClassID = 2 },
                 };

            var query = from game in library
                        select new
                        {
                            game.name,
                            game.damage
                        };

            Assert.Equal(5, query.Count());

            var firstMutant = query.First();
            Assert.Equal("Cyclop", firstMutant.name);
            Assert.Equal(39.47, firstMutant.damage);
        }
        [Fact]
        public void Test6()
        {
            //List<Mutant> mutantTeam = new List<Mutant>();

            //mutantTeam.Add(new Mutant(39.47, "Cyclop", false, 13, 1));
            //mutantTeam.Add(new Mutant(129.99, "Ninja Turtle", true, 23, 2));
            //mutantTeam.Add(new Mutant(50.29, "Mindmeld", true, 17, 3));
            //mutantTeam.Add(new Mutant(79.80, "Bio-morph", false, 20, 2));
            //mutantTeam.Add(new Mutant(101.2, "Frostbite", true, 21, 2));

            PerformSortByDamage(mutants);


        }
        [Fact]
        public void Test7()
        {
            //List<Mutant> mutantTeam = new List<Mutant>();

            //mutantTeam.Add(new Mutant(39.47, "Cyclop", false, 13, 1));
            //mutantTeam.Add(new Mutant(129.99, "Ninja Turtle", true, 23, 2));
            //mutantTeam.Add(new Mutant(50.29, "Mindmeld", true, 17, 3));
            //mutantTeam.Add(new Mutant(79.80, "Bio-morph", false, 20, 2));
            //mutantTeam.Add(new Mutant(101.2, "Frostbite", true, 21, 2));

            ConvertToArray(mutants);


        }
        [Fact]
        public void Test8()
        {
            //List<Mutant> mutantTeam = new List<Mutant>();

            //mutantTeam.Add(new Mutant(39.47, "Cyclop", false, 13, 1));
            //mutantTeam.Add(new Mutant(129.99, "Ninja Turtle", true, 23, 2));
            //mutantTeam.Add(new Mutant(50.29, "Mindmeld", true, 17, 3));
            //mutantTeam.Add(new Mutant(79.80, "Bio-morph", false, 20, 2));
            //mutantTeam.Add(new Mutant(101.2, "Frostbite", true, 21, 2));

            PerformSortListByName(mutants);


        }
        [Fact]
        public void Test9()
        {
            Mutant firstMutant = mutants.First();
            string mutantInfo = string.Format("Name: {0}, Damage: {1}, Able to Fight: {2}, Level: {3}, Class ID: {4}",
            firstMutant.name, firstMutant.damage, firstMutant.ableToFight, firstMutant.level, firstMutant.mutantClassID);
            Console.WriteLine(mutantInfo);

        }
        [Fact]
        public void Test10()
        {
            Console.WriteLine("Mutant Classes:");
            foreach (var mutantClass in mutantClasses)
            {
                Console.WriteLine($"ID: {mutantClass.id}, Name: {mutantClass.name}");
            }

            Console.WriteLine("\nMutants:");
            foreach (var mutant in mutants)
            {
                Console.WriteLine($"Name: {mutant.name}, Damage: {mutant.damage}, Able to Fight: {mutant.ableToFight}, Level: {mutant.level}, Class ID: {mutant.mutantClassID}");
            }

        }
        [Fact]
        //���������� � LINQ
        public void Test11()
        {
            var mutantsByClass = mutants.GroupBy(mutant => mutant.mutantClassID).ToDictionary(group => group.Key, group => group.ToList() );

            Assert.Equal(mutantClasses.Count, mutantsByClass.Count);

            foreach (var mutantClass in mutantClasses)
            {
                Assert.Contains(mutantClass.id, mutantsByClass.Keys);
            }

            foreach (var mutantList in mutantsByClass.Values)
            {
                foreach (var mutant in mutantList)
                {
                    Assert.Equal(mutant.mutantClassID, mutantList.First().mutantClassID);
                }
            }


        }

        [Fact]
        public void Test12()
        {
            SortedList<int, Mutant> sortedMutants = new SortedList<int, Mutant>();

            foreach (var mutant in mutants)
            {
                sortedMutants.Add(mutant.level, mutant);
            }

          

            var sortedMutantsList = sortedMutants.Values.ToList();
            for (int i = 0; i < sortedMutantsList.Count - 1; i++)
            {
                Assert.True(sortedMutantsList[i].level <= sortedMutantsList[i + 1].level, $"Mutants are not sorted correctly by level at index {i}.");
            }

            Queue<Mutant> mutantQueue = new Queue<Mutant>();

            foreach (var mutant in mutants)
            {
                mutantQueue.Enqueue(mutant);
            }

            while (mutantQueue.Count > 0)
            {
                var mutant = mutantQueue.Dequeue();
                Assert.NotNull(mutant);
            }
        }
        [Fact]
        public void Test13()
        {
            var groupedMutants = mutants.GroupBy(m => m.mutantClassID);

            Assert.Equal(mutantClasses.Count, groupedMutants.Count());

            foreach (var group in groupedMutants)
            {
                var mutantClassID = group.Key;
                var expectedCount = mutants.Count(m => m.mutantClassID == mutantClassID);
                Assert.Equal(expectedCount, group.Count());
            }
        }
        [Fact]
        public void Test14()
        {
            var groupedMutants = mutants.GroupBy(m => m.mutantClassID);

            var sortedGroups = groupedMutants.OrderByDescending(m => m.Count());

            var sortedMutants = sortedGroups.SelectMany(m => m).ToList();

            Assert.Equal(mutants.Count, sortedMutants.Count);

            int previousCount = int.MaxValue;
            foreach (var mutant in sortedMutants)
            {
                if (previousCount != int.MaxValue)
                {
                    Assert.True(mutants.Count(m => m.mutantClassID == mutant.mutantClassID) <= previousCount);
                }
                previousCount = mutants.Count(m => m.mutantClassID == mutant.mutantClassID);
            }
        }




        void PerformSelect(List<Mutant> mutants)
        {
            var query = mutants.Select(m => new { m.name, m.damage });

            int expectedCount = mutants.Count;

            int actualCount = query.Count();

            Assert.Equal(expectedCount, actualCount);
            Assert.Equal(query.First().name, "Cyclop");

        }

        void PerformSelect(Dictionary<int, Mutant> mutants)
        {
            var query = mutants.Select(m => new { m.Value.name, m.Value.damage });

            int expectedCount = mutants.Count;

            int actualCount = query.Count();

            Assert.Equal(expectedCount, actualCount);
        }

        void PerformWhere(List<Mutant> mutants)
        {
            var mutantsAbleToFight = mutants.Where(m => m.ableToFight).ToList();

            foreach (var mutant in mutantsAbleToFight)
            {
                Assert.True(mutant.ableToFight, $"The game {mutant.name} was filtered incorrectly.");
            }
        }

        void PerformWhere(Dictionary<int, Mutant> mutants)
        {
            var mutantsAbleToFight = mutants.Where(m => m.Value.ableToFight).ToList();

            Assert.Equal(3, mutantsAbleToFight.Count);
        }

        void PerformSortByDamage(List<Mutant> mutants)
        {
            var sortedMutants = mutants.OrderBy(mutant => mutant, new MutantDamageComparer()).ToList();

            for (int i = 0; i < sortedMutants.Count - 1; i++)
            {
                Assert.True(sortedMutants[i].damage <= sortedMutants[i + 1].damage, $"Mutants are not sorted correctly by damage at index {i}.");
            }
        }


        void ConvertToArray(List<Mutant> mutants)
        {
            Mutant[] mutantsArray = mutants.ToArray();

            Assert.Equal(mutants.Count, mutantsArray.Length);

            for (int i = 0; i < mutants.Count; i++)
            {
                Assert.Equal(mutants[i].name, mutantsArray[i].name);
                Assert.Equal(mutants[i].damage, mutantsArray[i].damage);
            }

        }
        void PerformSortListByName(List<Mutant> mutants)
        {
            var sortedMutants = mutants.OrderBy(mutant => mutant.name).ToList();
            Console.WriteLine("Sorted by name:");

            for (int i = 0; i < sortedMutants.Count - 1; i++)
            {
                Assert.True(string.Compare(sortedMutants[i].name, sortedMutants[i + 1].name) <= 0, $"Mutants are not sorted correctly by name at index {i}.");
            }
        }



    }

}